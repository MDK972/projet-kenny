<?php

require_once("connexion.php");

$valid = true;
if (count($_POST)) {
    $keys = ['firstname', 'lastname', 'birthdate', 'email', 'login', 'pwd'];
    foreach ($keys as $key) {
        if (!isset($_POST[$key])) {
            echo "La valeur $key n'a pas été transmise";
            $valid = false;
        }
    }
} else $valid = false;

if ($valid) {
    $regex = "/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
    if (!preg_match($regex, $_POST['email'])) {
        echo "Email au mauvais format";
        $valid = false;
    }

    /*$regex = '/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#?!@$%^&*-])(?=.{5,10})/';
    if (!preg_match($regex, $_POST['pwd'])) {
        echo "Mot de passe au mauvais format";
        $valid = false;
    }*/

    if ($valid) {
        $sql = "INSERT INTO users (firstname, lastname, birthdate, email, login, pwd) VALUES ('{$_POST['firstname']}', '{$_POST['lastname']}', '{$_POST['birthdate']}', '{$_POST['email']}', '{$_POST['login']}', '{$_POST['pwd']}')";
        $db->query($sql);
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription-cactus-jack</title>
</head>
<body>
<form method="POST" style="background-color:cornflowerblue ;">
    <fieldset> 
        <div>
            <label>Nom :</label> 
            <input  name="firstname" type="text">
        </div>
        <br>
        <div>
            <label>Prenom :</label> 
            <input  name="lastname" type="text">
        </div>
        <br>
        <div>
            <label>Date de Naissance :</label> 
            <input  name="birthdate" type="date">
        </div>
        <br>
        <div>
            <label>Email :</label> 
            <input  name="email" type="email">
        </div>
        <br>
        <div>
            <label>Login :</label> 
            <input  name="login" type="text">
        </div>
        <br>
        <div>
            <label>Mot de passe :</label> 
            <input  name="pwd" type="password">
        </div>
        <br>
        <input type="submit" value="S'inscrire" name="register">
    </fieldset>
</form>
</body>
</html>